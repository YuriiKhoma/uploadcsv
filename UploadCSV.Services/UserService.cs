﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UploadCSV.DataAccess.Abstractions;
using UploadCSV.DataAccess.Models.Entities;
using UploadCSV.DataAccess.Repositories;
using UploadCSV.Services.Abstractions;

namespace UploadCSV.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task<List<User>> GetAll()
        {
            var allUsers = userRepository.GetAll();
            return await allUsers;
        }

        public async Task<User> GetById(int id)
        {
            return await userRepository.GetById(id);
        }

        public async Task<User> GetByName(string name)
        {
            return await userRepository.GetByName(name);
        }

        public async Task<User> GetByPhone(string phone)
        {
            return await userRepository.GetByPhone(phone);
        }

        public async Task<ICollection<User>> UploadUsers(ICollection<User> users)
        {
            var result = new List<User>();

            foreach (var user in users)
            {
                result.Add(await userRepository.Add(user));
            }

            return result;
        }
    }
}
