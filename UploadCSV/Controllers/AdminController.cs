﻿using CsvHelper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UploadCSV.API.Adapters;
using UploadCSV.DataAccess.Models.Entities;
using UploadCSV.Services.Abstractions;

namespace UploadCSV.API.Controllers
{
    public class AdminController : Controller
    {
        private readonly IUserService userService;
        private readonly IUserAdapter userAdapter;

        public AdminController(IUserService userService, IUserAdapter userAdapter)
        {
            this.userService = userService;
            this.userAdapter = userAdapter;
        }

        [HttpGet]
        public IActionResult Index(List<User> users = null)
        {
            if (users == null)
            {
                users = new List<User>();
            }

            return View(users);
        }

        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file, [FromServices] IHostingEnvironment hostingEnvironment)
        {
            string fileName = $"{hostingEnvironment.WebRootPath}\\files\\{file.FileName}";

            using (FileStream fileStream = System.IO.File.Create(fileName))
            {
                file.CopyTo(fileStream);
                fileStream.Flush();
            }

            var users = GetUsersList(file.FileName);

            //Запхати users в БДшку
            await userService.UploadUsers(users);

            //Витягнути з БДшки колекцію юзерів usersList
            var usersList = await userService.GetAll();

            //повернути
            return Index(usersList);
        }

        public List<User> GetUsersList(string fileName)
        {
            List<User> users;

            var path = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\files"}" + "\\" + fileName;
            FileInfo f = new FileInfo(path);
            f.MoveTo(Path.ChangeExtension(path, ".xlsx"));
            users = userAdapter.GetFromExcell(f.ToString()).Result;
            return users;
        }
    }
}
