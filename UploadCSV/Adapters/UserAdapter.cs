﻿using CsvHelper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UploadCSV.DataAccess.Models.Entities;
using ExcelDataReader;

namespace UploadCSV.API.Adapters
{
    public class UserAdapter : IUserAdapter
    {
        public async Task<List<User>> GetFromExcell(string fileName)
        {
            var users = new List<User>();

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var IsFirstRow = true;

                    while (reader.Read()) //Each row of the file
                    {
                        if (IsFirstRow)
                            IsFirstRow = false;
                        else 
                        {
                            users.Add(new User
                            {
                                Name = reader.GetValue(0).ToString(),
                                DateOfBirth = Convert.ToDateTime(reader.GetValue(1)),
                                Maried = Convert.ToBoolean(reader.GetValue(2)),
                                Phone = reader.GetValue(3).ToString(),
                                Salary = Convert.ToUInt32(reader.GetValue(4))
                            });
                        }
                    }
                }
            }

            return users;
        }
    }
}
