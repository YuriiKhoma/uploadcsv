﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UploadCSV.DataAccess.Models.Entities;

namespace UploadCSV.API.Adapters
{
    public interface IUserAdapter
    {
        public Task<List<User>> GetFromExcell(string fileName);
    }
}
