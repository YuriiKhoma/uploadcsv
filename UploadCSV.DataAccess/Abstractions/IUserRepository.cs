﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UploadCSV.DataAccess.Models.Entities;

namespace UploadCSV.DataAccess.Abstractions
{
    public interface IUserRepository
    {
        public Task<User> Add(User user);
        public Task<User> GetById(int id);
        public Task<User> GetByName(string name);
        public Task<User> GetByPhone(string phone);
        public Task<List<User>> GetAll();

    }
}
