﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using UploadCSV.DataAccess.Models.Entities;

namespace UploadCSV.DataAccess.Configuration
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public UserContext(DbContextOptions<UserContext> options) : base(options)
        { }
    }
}
