﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UploadCSV.DataAccess.Models.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool Maried { get; set; }
        public string Phone { get; set; }
        public decimal Salary { get; set; }
    }
}
