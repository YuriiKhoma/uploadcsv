﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UploadCSV.DataAccess.Abstractions;
using UploadCSV.DataAccess.Configuration;
using UploadCSV.DataAccess.Models.Entities;
using System.Linq;

namespace UploadCSV.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserContext context;

        public UserRepository(UserContext context)
        {
            this.context = context;
        }

        public async Task<User> Add(User user)
        {
            await context.Users.AddAsync(user);
            await context.SaveChangesAsync();
            return user;
        }

        public async Task<List<User>> GetAll()
        {
            return await context.Users.ToListAsync();
        }

        public async Task<User> GetById(int id)
        {
            return await context.Users.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<User> GetByName(string name)
        {
            return await context.Users.FirstOrDefaultAsync(u => u.Name == name);
        }

        public async Task<User> GetByPhone(string phone)
        {
            return await context.Users.FirstOrDefaultAsync(u => u.Phone == phone);
        }
    }
}
