﻿using System;
using System.Collections.Generic;

namespace UploadCSV.IoC
{
    public static class IoC
    {
        private static Dictionary<Type, Type> scopes = new Dictionary<Type, Type>();

        public static void AddScopped<TAbstraction, TImplementation>()
        {
            var key = typeof(TAbstraction);

            if (!scopes.ContainsKey(key))
                scopes.Add(key, typeof(TImplementation));
        }

        public static Dictionary<Type, Type> GetScopes()
        {
            return scopes;
        }

    }
}
